
// Playing Cards
// Jesse Dorin

#include <iostream>
#include <conio.h>

using namespace std;

enum class Suit
{
	HEARTS,
	CLUBS,
	DIAMONDS,
	SPADES
};
enum class Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};
struct Card
{
	Rank Rank;
	Suit Suit;
};

int main()
{
	
	Card c1;
	c1.Rank = Rank::KING;
	c1.Suit = Suit::CLUBS;



	_getch();
	return 0;
}
